//Используя ряд sinx=x-(x^3 / 3!)+(x^5 / 5!)-(x^7 / 7!)+... рассчитать значение синуса x
//с заданной точностью t (например, 10^(-3) ). Заданную точность считать достигнутой, если
//очередной элемент ряда меньше заданной точности.



#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	double x,y,d; 
	d=360.00;
	cout<<"Введите угол X в градусах"<<endl;
	cin>>y; 
	if (cin.fail()) {cout <<"Введено некорректное число"<<endl; system("pause"); return 1;}
	x=fmod(y,d);
	x=(x*M_PI/180);
	double t; 
	cout<<"Введите точность t"<<endl;
	cin>>t;
	if (cin.fail()) {cout <<"Введено некорректное число"<<endl; system("pause"); return 2;}
	double newx; 
	double sinx; 
	int faktor; 
	int i; 
	int c; 
	faktor=1; i=1; c=1; newx=x; sinx=0;
	cout<<"sin("<<x<<" рад) = ";
	while ((fabs(newx/faktor))>=t) {
	    if (c>0) {
		    if (i==1) {
                cout<<" ";
			    } else {
				cout<<" + ";
				}
            } else {
			cout<<" - ";
            }
		   cout<<"( "<<newx<<" / "<<faktor<<" )";
           sinx+=(c*(newx/faktor));
		   newx*=x*x;
		   i+=2;;
		   faktor=faktor*(i-1)*i;
		   c*=-1;
		   }	
	cout<<" = "<<sinx<<endl;
	system("pause");
	return 0;
}
